# Light management system 

light management system is always in the market.we are making a smart light management system with fully open source with a centralized controller section and which using anywhere if you have a suffiecient net connection.
![smart_light_management](https://gitlab.com/manumsm2/smart_light_management/-/raw/master/Smart_Light_Management/Images/Image_3.jpg)


# List of Parameters 

- ON-OFF switch
- Find out current status of light
- Easy control light on and off.
- If there is a manual switch we can use both manual and smart mode
- Even for smart section channel damage you can use light with manual channel mode



# Prerequisites

- Node MCUESP32 (wroom32)
- HI-LINK
- RELAY MODULE
- WIRE
- SERVER (YOU CAN USE ANY ONLINE PLATEFORM LIKE SERVER ASK sensor for beginning)

# SOFTWARE SUPPORTS

- Arduino ide
- mqtt client and subscriber
- Node red


# Getting Started

To download arduino ide and run esp32 library files and compile code of esp32wroom
the mqtt download into the system.
subscribe the device.
- Download node red
- Create dashboard
- Control device

# Contributing

Instructions coming up soon.

# License

This project is licensed under the MIT License - see the LICENSE.md file for details


